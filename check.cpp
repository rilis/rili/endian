#include <iostream>
#include <rili/Endian.hpp>

int main(int argc, char** argv) {
#ifdef RILI_ENDIAN_LITTLE
    std::cout << "RILI_ENDIAN_LITTLE defined" << std::endl;
#endif

#ifdef RILI_ENDIAN_BIG
    std::cout << "RILI_ENDIAN_BIG defined" << std::endl;
#endif

    return 0;
}