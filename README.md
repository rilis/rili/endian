# Rili Endian

This is module of [rili](https://gitlab.com/rilis/rili) which contain implementation of cross-platform, cross-compiler endianess conversion routines.

**Documentation and API reference** :  [rilis.io](https://rilis.io/projects/rili/library/endian)

**[Issue tracker](https://gitlab.com/rilis/rilis/issues)**

**Project status**

[![build status](https://gitlab.com/rilis/rili/endian/badges/master/build.svg)](https://gitlab.com/rilis/rili/endian/commits/master)

